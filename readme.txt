Afin de transférer les hooks sur votre dépôt local, nous vous invitons à vous rendre dans le dossier .git/hooks et copier le fichier pre-commit.sample en un fichier pre-commit.

Dedans, ajoutez ce code puis enregistrez le fichier : 

#!/bin/bash

# Change the following values to suit your local setup.
# Add this file on your .git/hooks/ and name it as 'pre-commit'

# The path relative to the repository root in which to store the commit info file.
COMMIT_INFO_PATH=suivi/commitInfo.txt

read -p "Stocker un fichier complémentaire contenant le texte 'commit vérifié le <date et heure du jour>' (y/[n]) ?" yn < /dev/tty
yn=${yn:n}

case $yn in
[Yy]* )
  echo "commit vérifié le $(date)" > $COMMIT_INFO_PATH
  exit 0;;
[Nn]* )
  exit 0;;
esac
exit 0

